var express = require('express');
var mongo = require('mongodb');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bodyParser = require('body-parser');
var cors = require('cors');
var axios = require('axios');



var reportSchema = new Schema({
    year:{
        type: String,
    }, 
    petroleum_product:{
        type:String
    },
    sale: Number
  });
  
var reportModel = mongoose.model('Report', reportSchema);

var app = express();

// Basic Configuration 
var port = 3000;
mongoose.connect("mongodb+srv://hams:buttsoup@cluster0.15fmb.mongodb.net/test?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

/** this project needs a db !! **/ 
// mongoose.connect(process.env.DB_URI);

app.use(bodyParser.urlencoded({extended: false}));

app.use(cors());

app.get('/', function (req, res) {
   res.send('Hello World');
});

app.get('/fetchdata', function(req, res) {
    axios.get('https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json')
    .then(response => {
        //res.json({data: response.data});
        reportModel.create(response.data, function(err, reports){
            if(err){
                throw err;
            }

            res.json({report:reports});
        })
        //res.json({data:response.data});
    })
    .catch(error => {
        console.log(error);
    });
});



app.get('/create', function(req, res) {
    reportModel.create({year:"2020", petroleum_product: "diseel", sale:2343}, function(err, data){
        if(err){
            throw err;
        }
        res.json({data:data});
    })
})

function printReport() {
    reportModel.find({}, function(err, data){
        if(err){
            throw err;
        }
    
        var products = [];
        var counter = 0;
        for(var i=0; i<data.length; i++) {
            for(var j=0; j<products.length; j++){
            if(data[i].petroleum_product == products[j]){
            counter++;
            }
        }
        if(counter == 0){
            products.push(data[i].petroleum_product);
        }
        counter=0;
        }
    
        var report = [];
        var r1min = 1000000, r1max = 0, r1avg = 0, r1sum=0;
        var r2min = 1000000, r2max = 0, r2avg = 0, r2sum=0;
        var r3min = 1000000, r3max = 0, r3avg = 0, r3sum=0;
        for(var i=0; i<products.length; i++){
            for(var j = 0; j<data.length; j++) {
                if(products[i] == data[j].petroleum_product) {
                    if(parseInt(data[j].year) >= 2000 && parseInt(data[j].year) <=2004) {
                        if(data[j].sale <= r1min){
                            r1min = data[j].sale;
                        }
                        if(data[j].sale >= r1max) {
                            r1max = data[j].sale;
                        }
                        r1sum = r1sum + data[j].sale;
                    }
                    else if(parseInt(data[j].year) >= 2005 && parseInt(data[j].year) <=2009) {
                        if(data[j].sale <= r2min){
                            r2min = data[j].sale;
                        }
                        if(data[j].sale >= r2max) {
                            r2max = data[j].sale;
                        }
                        r2sum = r2sum + data[j].sale;
                    }
                    else if(parseInt(data[j].year) >= 2010 && parseInt(data[j].year) <=2014) {
                        if(data[j].sale <= r3min){
                            r3min = data[j].sale;
                        }
                        if(data[j].sale >= r3max) {
                            r3max = data[j].sale;
                        }
                        r3sum = r3sum + data[j].sale;
                    }
                }
            }
            r1avg = r1sum/5;
            r2avg = r2sum/5;
            r3avg = r3sum/5;
            report.push({
                "product": products[i],
                "year": "2000-2004",
                "min":r1min,
                "max":r1max,
                "avg":r1avg
            });
            report.push({
                "product": products[i],
                "year": "2004-2009",
                "min":r2min,
                "max":r2max,
                "avg":r2avg
            });
            report.push({
                "product": products[i],
                "year": "2010-2014",
                "min":r3min,
                "max":r3max,
                "avg":r3avg
            });
    
            r1max = r1avg = r1sum = r2max = r2avg = r2sum = r3max = r3avg = r3sum = 0;
            r1min = r2min = r3min = 1000000;
        }
        console.table(report);
        // res.json({data: report});
        });
}

var server = app.listen(3000, function () {
   
   console.log("Example app listening at localhost://3000");
   
   printReport();
})